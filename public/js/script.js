$(document).ready(function(){
    $('#slider').flexslider({
        animation:"slide"
    });

    //Hover copy
    $('#content nav a').hover(
        function() {
            var copy = $(this).find('img').attr('alt');
            $(this).prepend( $( "<div>"+copy+"</div>" ) );
        }, function() {
            $(this).find( "div:last" ).remove();
        }
    );
})
