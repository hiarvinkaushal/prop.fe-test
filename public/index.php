<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <link rel="stylesheet" href="styles.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
</head>

<body>
    <header>
        <img src="images/logo.png" alt="PBT"/>
        <nav>
            <ul>
                <li><a href="#">About</a></li>
                <li><a href="#">Menus</a></li>
                <li><a href="#">Locations</a></li>
                <li><a href="#">Bookings</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
    </header>

<nav id="social">
    <ul>
        <li><a href="#"><img src="images/facebook.png" alt="Facebook"/></a></li>
        <li><a href="#"><img src="images/twitter.png" alt="Twitter"/></a></li>
        <li><a href="#"><img src="images/instagram.png" alt="Instagram"/></a></li>
    </ul>
</nav>
<div id="content">
    <div id="heading">
        <h1>Welcome to PBT,<br>Propeller!</h1>
        <h2>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </h2>
    </div>
    <nav>
        <ul>
            <li>
                <a href="#"><img src="images/book-table.jpg" alt="Book a table"/></a>
            </li>
            <li>
                <a href="#"><img src="images/menus.jpg" alt="View menus"/></a>
            </li>
            <li>
                <a href="#"><img src="images/christmas.jpg" alt="Book for Christmas"/></a>
            </li>
            <li>
                <a href="#"><img src="images/contact.jpg" alt="Contact us"/></a>
            </li>
        </ul>
    </nav>
    <div id="slider">
        <ul class="slides">
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
            <li><img src="images/slider.jpg" alt="Thank you"/></li>
        </ul>
    </div>
</div>
<footer>
    <article id="twitter">
        <h3>Twitter</h3>
        <p>@PBT_propeller</p>
        <a href="#" class="btn">Follow</a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<span>an hour ago</span></p>

        <?php
/*            require_once("twitteroauth/twitteroauth.php");
            $consumerkey = "EA4XvFHuNcBrlvb1PREeA";
            $consumersecret = "yHjt5omz1JB25849akOjtAOLDhR52PFc1d4naQC4HI";
            $accesstoken = " 81891561-H6sMeEKqwi3qAwlr7VLfODLY2toUzlTtNPmu2zifC";
            $accesstokensecret = "UPiloDwBKpqU5LxP2VwHvp6wquW0OSTfE9KCFpKr0";
            $query = "https://api.twitter.com/1.1/statuses/user_timeline.json?exclude_replies=true&include_rts=false&screen_name=propellercomms&count=1";

            function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret){
                $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
                return $connection;
            }
                $connection = getConnectionWithAccessToken($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);
                $tweets = $connection->get($tweetQuery);
                var_dump($tweets);*/

        ?>
    </article>
    <article id="instagram">
        <h3>Instagram</h3>
        <p>PBT_propeller</p>
        <a href="#" class="btn">Follow</a>
        <ul>
            <li>
                <img src="images/insta1.jpg" alt=""/>
            </li>
            <li>
                <img src="images/insta2.jpg" alt=""/>

            </li>
        </ul>
    </article>
    <article id="form">
        <h3>Sign up</h3>
        <p>Subscribe our newsletter and never miss out <br> our exclusive offers!</p>

        <form action="post">
            <fieldset>
                <ul>
                    <li>
                        <label for="name">Name</label><input name="name" type="text"/>
                    </li>
                    <li>
                        <label for="email">Email</label><input name="email" type="email"/>
                    </li>
                    <li>
                        <label for="birthday">Birthday</label>
                        <select name="day" id="day" onchange="" size="1">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                        <select name="month" id="month" onchange="" size="1">
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>


                    </li>
                </ul>
                <input type="submit" value="Submit" class="btn"/>
            </fieldset>
        </form>
    </article>
</footer>
<div id="address">
    <ul>
        <li>
            PBT Propeller
        </li>
        <li>
            1-3 Windsor court
        </li>
        <li>
            Rugby
        </li>
        <li>
            Warwickshire
        </li>
        <li>
            CV21 3BH
        </li>
        <li>
            pbt@propcom.co.uk
        </li>
        <li>
            01788 566270
        </li>
        <li>
            <a href="#"><img src="images/facebook-foot.png" alt="Facebook"/></a>
            <a href="#"><img src="images/twitter-foot.png" alt="Twitter"/></a>
            <a href="#"><img src="images/instagram-foot.png" alt="Instagram"/></a>
        </li>
    </ul>
</div>
<script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>